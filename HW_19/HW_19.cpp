

#include <iostream>
#include <string>
using namespace std;

class Animal
{
public:
    virtual void Voice()
    {
        cout << "Animal Class" << endl;
    }
};
class Monkey : public Animal
{
    void Voice() override
    {
        cout << "Monkey: banana! " << endl;
    }
};
class cock : public Animal
{
    void Voice() override
    {
        cout << "cock: Ku-ka Re-ku! " << endl;
    }
};
class owl : public Animal
{
    void Voice() override
    {
        cout << "owl: Wouw-Wouw! " << endl;
    }
};
class tiger : public Animal
{
    void Voice() override
    {
        cout << "tiger: Rrrrar! " << endl;
    }
};
class elephant : public Animal
{
    void Voice() override
    {
        cout << "elephant: Wooouuuuu! " << endl;
    }
};



int main()
{
    
    Animal* Animals[5] =
    {
        new Monkey, new cock, new owl, new tiger, new elephant
    };
    
    for (Animal* i : Animals)
    {
        i->Voice();
    }

    return 0;
}

